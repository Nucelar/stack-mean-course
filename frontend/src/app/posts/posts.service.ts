import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Post } from './post.model';
import { environment } from '../../environments/environment';
import { PostsSocketsService } from './posts-sockets.service';
import { AuthService } from '../auth/auth.service';

const BACKEND_URL = environment.apiUrl + "/posts/";

@Injectable({ providedIn: 'root' })
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<{ posts: Post[]; postCount: number }>();

  constructor(private http: HttpClient, private router: Router, private postsSocketsService: PostsSocketsService) {
    this.observePostSocket();
  }

  getPosts(postsPerPage: number, currentPage: number) {
    const queryParams = `?pagesize=${postsPerPage}&page=${currentPage}`;
    this.http
      .get<{ message: string; posts: any; maxPosts: number }>(
        BACKEND_URL + queryParams
      )
      .pipe(
        map((postData) => {
          return {
            posts: postData.posts.map((post) => {
              return {
                title: post.title,
                content: post.content,
                id: post._id,
                imagePath: post.imagePath,
                creator: post.creator,
              };
            }),
            maxPosts: postData.maxPosts,
          };
        })
      )
      .subscribe((transformedPostsData) => {
        console.log(transformedPostsData);
        this.posts = transformedPostsData.posts;
        this.postsUpdated.next({
          posts: [...this.posts],
          postCount: transformedPostsData.maxPosts,
        });
      });
    return [...this.posts];
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  getPost(id: string) {
    return this.http.get<{
      _id: string;
      title: string;
      content: string;
      imagePath: string;
      creator: string;
    }>(BACKEND_URL + id);
  }

  addPost(title: string, content: string, image: File) {
    const postData = new FormData();
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image, title);
    this.http
      .post<{ message: string; post: Post }>(
        BACKEND_URL,
        postData
      )
      .subscribe((responseData) => {
        console.log(postData.get("title"));
        this.postsSocketsService.emitCreatePostSocket(postData);
        this.router.navigate(['/']);
      });
  }

  updatePost(
    id: string,
    title: string,
    content: string,
    image: File | string
  ) {
    let postData: Post | FormData;
    if (typeof image === 'object') {
      postData = new FormData();
      postData.append('id', id);
      postData.append('title', title);
      postData.append('content', content);
      postData.append('image', image, title);
    } else {
      postData = {
        id: id,
        title: title,
        content: content,
        imagePath: image,
        creator: null,
      };
    }
    this.http
      .put(BACKEND_URL + id, postData)
      .subscribe((response) => {
        this.postsSocketsService.emitUpdatePostSocket(postData);
        this.router.navigate(['/']);
      });
  }

  deletePost(postId: string) {
    return this.http.delete(BACKEND_URL + postId);
  }

  emitDeleteSocket(postId: string) {
    console.log("emitDeleteSocket");
    this.postsSocketsService.emitDeletePostSocket(postId);
  }

  private observePostSocket() {
    console.log("Waiting for sockets");
    this.postsSocketsService.receiveCreatePostSocket()
    .subscribe((post: any) => {
      console.log(`Create ${post.id} Post socket received`);
      this.refreshPosts();
    });

    this.postsSocketsService.receiveUpdatePostSocket()
    .subscribe((post: any) => {
      console.log(`Update ${post.id} Post socket received`);
      this.refreshPosts();
    });

    this.postsSocketsService.receiveDeletePostSocket()
    .subscribe((postId: string) => {
      console.log(`Delete ${postId} Post socket received`);
      this.refreshPosts();
    });
  }

  private refreshPosts() {
    this.getPosts(10, 0);
  }
}
