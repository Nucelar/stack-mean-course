import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { AngularMaterialModule } from '../angular-material.module';
import { AppRoutingModule } from '../app-routing.module';

import { PostCreateComponent } from './post-create/post-create.component';
import { PostListComponent } from './post-list/post-list.component';

import { PostsSocketsService } from './posts-sockets.service';

import { environment } from '../../environments/environment';

const socketIOConfig: SocketIoConfig = {
  url: environment.socketEndpoint, options: {}
}

@NgModule({
  declarations: [PostCreateComponent, PostListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    SocketIoModule.forRoot(socketIOConfig),
    AngularMaterialModule,
    AppRoutingModule,
  ],
  providers: [PostsSocketsService]
})
export class PostsModule {}
