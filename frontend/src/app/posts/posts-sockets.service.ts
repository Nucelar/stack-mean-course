import { Injectable } from '@angular/core';
import {Socket} from 'ngx-socket-io';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Post } from './post.model';

@Injectable()
export class PostsSocketsService {

  constructor(private socket: Socket) {}

  emitCreatePostSocket(post: Post | FormData) {
    this.socket.emit("createPost", post);
  }

  receiveCreatePostSocket() {
    return new Observable((observer: any) => {
      this.socket.fromEvent('createPost').pipe(map((post: Post | FormData) => {
        observer.next(post);
      }));
    });
  }

  emitUpdatePostSocket(post: Post | FormData) {
    this.socket.emit('updatePost', post);
  }

  receiveUpdatePostSocket() {
    return new Observable((observer: any) => {
      this.socket.fromEvent('updatePost').pipe(map((post: Post | FormData) => {
        observer.next(post);
      }));
    });
  }

  emitDeletePostSocket(postId: string) {
    this.socket.emit('deletePost', postId);
  }

  receiveDeletePostSocket() {
    return new Observable((observer: any) => {
      this.socket.fromEvent('deletePost').pipe(map((postId: string) => {
        console.log("Recieved delete post socket");
        observer.next(postId);
      }));
    });
  }
}
